package models

import (
	"context"
	"log"
	"time"
)

func Consume(ctx context.Context, location chan Location) {
	agent := Agent{}
	for {
		select {
		case <-ctx.Done():
			log.Print("Received a DONE message. Exitting consumer")
			return
		case l := <-location:
			free_agents := agent.GetFreeAgents()
			if len(free_agents) > 0 {
				operator_agent := agent.GetClosestTo(l.X, l.Y, free_agents)
				log.Print("worker found :", operator_agent)
				operator_agent.SetTarget(l)
				go operator_agent.MoveAllTheWay()

			} else {
				time.Sleep(time.Second)
				location <- Location{l.X, l.Y}
			}
		}
	}
}
