package models

import (
	"encoding/json"
	"log"
	"math"
	"time"
)

type Location struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}
type Agent struct {
	ID     int       `json:"id"`
	X      float64   `json:"x"`
	Y      float64   `json:"y"`
	Free   bool      `json:"free"`
	Target *Location `json:"-"`
}

const MovementPerSecond = 1
const AgentWorkerCount = 5

var agent_repo *AgentRepo

func init() {
	agent_repo = NewAgentRepo()
	InitialAgents(AgentWorkerCount)
}

//initial the agents in the first run
func InitialAgents(count int) bool {
	for i := 0; i < count; i++ {
		a := Agent{
			ID:     i + 1,
			X:      0,
			Y:      0,
			Free:   true,
			Target: nil,
		}
		if err := a.Save(); err != nil {
			return false
		}
		log.Print("Agent #", a.ID, " has been created.")
	}
	return true
}

func (a *Agent) GetCurrentLocation() (float64, float64) {
	return a.X, a.Y
}
func (a *Agent) SetCurrentLocation(x, y float64) error {
	a.X = x
	a.Y = y
	if err := a.Save(); err != nil {
		return err
	}
	log.Print("Location of Agent #", a.ID, " Changed to: (", x, ", ", y, ")")
	return nil
}
func (a *Agent) GetDistanceFrom(x, y float64) float64 {
	x0, y0 := a.GetCurrentLocation()
	squares_sum := math.Pow(x-x0, 2) + math.Pow(y-y0, 2)
	return math.Sqrt(squares_sum)
}
func (a *Agent) GetDistanceFromTarget() float64 {
	return a.GetDistanceFrom(a.Target.X, a.Target.Y)
}
func (a *Agent) GetTimeDistanceFrom(x, y float64) float64 {
	distance := a.GetDistanceFrom(x, y)
	return distance / MovementPerSecond
}
func RoundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}
func (a *Agent) GetTimeDistanceFromTarget() float64 {
	time_distance := a.GetDistanceFromTarget() / MovementPerSecond
	return RoundFloat(time_distance, 3)
}
func (a *Agent) GetTarget() (float64, float64) {
	return a.Target.X, a.Target.Y
}
func (a *Agent) SetTarget(location Location) error {
	a.MakeBusy()
	a.Target = &location
	if err := a.Save(); err != nil {
		return err
	}
	log.Print("Target of Agent #", a.ID, " Changed to: (", location.X, ", ", location.Y, ")")
	return nil

}
func (a *Agent) SetFree() error {
	a.Target = nil
	a.Free = true
	if err := a.Save(); err != nil {
		return err
	}
	log.Print("Status of Agent #", a.ID, " Changed to FREE")
	return nil

}
func (a *Agent) IsFree() bool {
	return a.Free
}
func (a *Agent) MakeBusy() error {
	a.Free = false
	if err := a.Save(); err != nil {
		return err
	}
	log.Print("Status of Agent #", a.ID, " Changed to BUSY")
	return nil
}

// returns ok, reached
func (a *Agent) MoveOneStep() (ok bool, reached bool) {
	log.Print("Moving Agent #", a.ID, " one step...")
	time_distance := a.GetTimeDistanceFromTarget()
	if time_distance == 0 {
		log.Print("Moving Agent #", a.ID, " -- its already in the location")
		return true, true
	}
	x0, y0 := a.GetCurrentLocation()
	x, y := a.GetTarget()
	x_movement := (x - x0) / time_distance
	y_movement := (y - y0) / time_distance
	next_step_x := x0 + x_movement
	next_step_y := y0 + y_movement
	if err := a.SetCurrentLocation(next_step_x, next_step_y); err != nil {
		return false, false
	}
	if next_step_x == x && next_step_y == y {
		//target is reached
		log.Print("Agent #", a.ID, " has reached the target")
		if err := a.SetFree(); err != nil {
			return false, true
		}
		return true, true
	}
	return true, false
}
func (a *Agent) MoveAllTheWay() bool {
	for {
		t := a.GetTimeDistanceFromTarget()
		if t >= 1 {
			log.Print("Agent #", a.ID, " is going to wait 1 second for the next step")
			time.Sleep(time.Second * 1)
		} else {
			// It is the last step with less than 1 unit of distance,
			// so the waiting time is a fraction of a second
			time.Sleep(time.Duration(t) * time.Second)
		}
		ok, reached := a.MoveOneStep()
		if !ok {
			return false
		}
		if reached {
			break
		}
	}
	return true
}

func (a *Agent) ToJson() []byte {
	json_agent, err := json.Marshal(a)
	if err != nil {
		log.Print("Error converting struct to json : ", err.Error())
		return nil
	}
	return json_agent
}
func (a *Agent) JsonToStruct(json_agent []byte) *Agent {
	if err := json.Unmarshal(json_agent, a); err != nil {
		log.Print("Error converting json to agent: ", err.Error())
		return nil
	}
	return a
}

func (a *Agent) GetAll() []*Agent {
	agent := Agent{}
	agents := []*Agent{}
	for i := 0; i < AgentWorkerCount; i++ {
		new_agent := agent.RetrieveAgent(i + 1)
		agents = append(agents, new_agent)
	}
	return agents
}

func (a *Agent) GetFreeAgents() []*Agent {
	agents := a.GetAll()
	free_agents := []*Agent{}
	for _, agent := range agents {
		if agent.IsFree() {
			free_agents = append(free_agents, agent)
		}
	}
	return free_agents
}
func (a *Agent) GetClosestTo(x float64, y float64, agents []*Agent) *Agent {
	closest_agent := &Agent{}
	for _, agent := range agents {
		if closest_agent.ID == 0 || agent.GetTimeDistanceFrom(x, y) < closest_agent.GetTimeDistanceFrom(x, y) {
			closest_agent = agent
		}
	}
	return closest_agent
}

func (a *Agent) Save() error {
	agent_repo.Create(a)
	return nil
}
func (a *Agent) RetrieveAgent(id int) *Agent {
	return agent_repo.Get(id)
}
