package models

import (
	"sync"
)

type AgentRepo struct {
	agents map[int]*Agent
	mutex  sync.RWMutex
}

func NewAgentRepo() *AgentRepo {
	return &AgentRepo{
		agents: make(map[int]*Agent),
	}
}

func (ar *AgentRepo) Get(id int) *Agent {
	ar.mutex.RLock()
	defer ar.mutex.RUnlock()
	if agent, ok := ar.agents[id]; ok {
		return agent
	}
	return nil
}

func (ar *AgentRepo) Create(agent *Agent) *Agent {
	ar.mutex.Lock()
	defer ar.mutex.Unlock()
	ar.agents[agent.ID] = agent
	return agent
}
