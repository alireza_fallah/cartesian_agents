#!/usr/bin/env bash

set -e

CMD=$(which server)
PORT="${PORT:-8888}"

if [[ "$1" == "" ]]; then
    exec "${CMD}" -port="${PORT}"
else
    exec "$@"
fi
