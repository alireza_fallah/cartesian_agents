package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/alireza_fallah/cartesian_agents/models"
)

type LocationParam struct {
	X float64 `json:"x" binding:"required"`
	Y float64 `json:"y" binding:"required"`
}

func MoveTo(location_channel chan models.Location) gin.HandlerFunc {
	return func(c *gin.Context) {
		params := &LocationParam{}
		if err := c.ShouldBindJSON(params); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Validation failed: " + err.Error()})
			return
		}
		location := models.Location{
			X: params.X,
			Y: params.Y,
		}
		location_channel <- location
		c.JSON(http.StatusOK, gin.H{"success": "true"})
		return
	}
}
func ListAllAgents(c *gin.Context) {
	a := models.Agent{}
	all_agents := a.GetAll()
	c.JSON(http.StatusOK, all_agents)
	return
}
