package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alireza_fallah/cartesian_agents/models"
)

var API_PATH_V1 string = "/api/v1"

func makeRoutes(r *gin.Engine, location_channel chan models.Location) {
	agents_api := r.Group(API_PATH_V1 + "/agent")
	buildAgentRoutes(agents_api, location_channel)
}

func buildAgentRoutes(r *gin.RouterGroup, location_channel chan models.Location) {
	r.POST("/move", MoveTo(location_channel))
	r.GET("/list", ListAllAgents)
}
