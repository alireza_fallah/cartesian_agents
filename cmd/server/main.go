package main

import (
	"context"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/alireza_fallah/cartesian_agents/models"
)

func init() {
	if os.Getenv("RUNMODE") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}
}

func main() {
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	migrate()
	var LocationChannel = make(chan models.Location)
	makeRoutes(r, LocationChannel)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go models.Consume(ctx, LocationChannel)
	log.Fatal(r.Run(":" + os.Getenv("PORT")))
	<-ctx.Done()
}
